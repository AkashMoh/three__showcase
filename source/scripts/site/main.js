$(window).on('load', function () {
    $('body').addClass('is-loaded');
});

//canvas
const canvas = document.querySelector('.showcase');
const loadingScreen = document.querySelector('.loading__screen');
const controls = document.querySelector('.controls');
let loading = document.getElementById('loading');
let loaded = false;
let enter = document.getElementById("enter");
document.addEventListener("keyup", function (event) {
    if (event.code === 'Enter' && loaded) {
        loadingScreen.style.display = "none";
        controls.style.display = "block";

        let tl = gsap.timeline({
            ease: "expo.out"
        });
        tl.to(camera.position, { duration: 4, x: 3.5, y: 1.16, z: 6.88 });
        tl.to(camera.rotation, { duration: 3, y: 0.9 }, "-=2");
    }
});
document.addEventListener("keydown", function (event) {
    if (event.key === "ArrowRight" && loaded) {
        let tl = gsap.timeline();
        tl.to(camera.position, { duration: 3, x: 18, y: 1.16, z: 3.5 });
    }
    if (event.key === "ArrowLeft" && loaded) {
        let tl = gsap.timeline({
            ease: "expo.out"
        });
        tl.to(camera.position, { duration: 3, x: 3.5, y: 1.16, z: 6.88 });
        tl.to(camera.rotation, { duration: 3, y: 0.9 }, "-=2");
    }
});

//setup scene, camera and renderer for three js
const scene = new THREE.Scene();
// attributes (field of view in degrees, aspect ratio, near clipping plane, far clipping pane)
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 500);

const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));

//the below line adds canvas inside .cube
$(".showcase__canvas").append(renderer.domElement);

//window resize
window.addEventListener('resize', () => {
    //update camera
    camera.aspect = window.innerWidth / window.innerHeight;
    //update projection matrices
    camera.updateProjectionMatrix()
    //update renderer
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
})

//fullscreen
window.addEventListener('dblclick', () => {
    const fullscreenElement = document.fullscreenElement || document.webkitFullscreenElement;
    if (!fullscreenElement) {
        {
            if (canvas.requestFullscreen) {
                canvas.requestFullscreen()
            }
            else if (canvas.webkitRequestFullscreen) {
                canvas.webkitRequestFullscreen()
            }
        }
    }
    else {
        if (document.exitFullscreen) {
            document.exitFullscreen()
        }
        else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen()
        }
    }
});


// //controls
// const controls = new THREE.OrbitControls(camera, canvas);
// controls.enableDamping = true;

//axes helper to visualize xyz axes
const axesHelper = new THREE.AxesHelper(3);

camera.position.z = 3;
camera.position.y = 19;

//directional light
const directionalLight = new THREE.DirectionalLight(0xffffff, 50);
directionalLight.position.set(1, 1, 0);
directionalLight.castShadow = true;
scene.add(directionalLight);

//ambient light
const aLight = new THREE.AmbientLight(0x404040, 30);
scene.add(aLight);

// fog
scene.background = new THREE.Color(0x8E8E8E);
scene.fog = new THREE.Fog(0x8E8E8E, 0.2, 15);


//plane
const planeGeometry = new THREE.PlaneGeometry(1, 1);
const planeMaterial = new THREE.MeshStandardMaterial({ color: 0x000000, side: THREE.DoubleSide });
const plane = new THREE.Mesh(planeGeometry, planeMaterial);
plane.rotation.x = Math.PI * 0.5;
plane.scale.set(500, 500, 500);
// plane.position.set(0, 0, 0);
scene.add(plane);

//loading manager
const manager = new THREE.LoadingManager();
manager.onStart = function (url, itemsLoaded, itemsTotal) {
    console.log('Started loading file: ' + url + '.\nLoaded ' + itemsLoaded + ' of ' + itemsTotal + ' files.');
};
manager.onLoad = function () {
    console.log('Loading complete!');
    loaded = true;
    enter.style.visibility = "visible";
};
manager.onProgress = function (url, itemsLoaded, itemsTotal) {
    console.log('Loading file: ' + url + '.\nLoaded ' + itemsLoaded + ' of ' + itemsTotal + ' files.');
    console.log((itemsLoaded / itemsTotal) * 100);
    loading.textContent = (itemsLoaded / itemsTotal) * 100 + " %";
};
manager.onError = function (url) {
    console.log('There was an error loading ' + url);
};

//load car model
const loader = new THREE.GLTFLoader(manager);

datsun = new THREE.Object3D();
loader.load('images/datsun/scene.gltf', function (gltf) {
    datsun = gltf.scene.children[0];
    datsun.position.set(10, 10.95, 0);
    scene.add(datsun);
    tick();
});

porsche = new THREE.Object3D();
loader.load('images/porsche/scene.gltf', function (gltf) {
    porsche = gltf.scene.children[0];
    porsche.scale.set(0.6, 0.6, 0.6);
    porsche.position.set(15, -0.05, 0);
    scene.add(porsche);
    tick();
});


//clock
// const clock = new THREE.Clock();

//clock()
function tick() {
    // time
    // const elapsedTime = clock.getElapsedTime()
    requestAnimationFrame(tick);

    //damping update()
    // controls.update()

    renderer.render(scene, camera);
}
tick();